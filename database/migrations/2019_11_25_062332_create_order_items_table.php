<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->integer('quantity');
            $table->decimal('price',5,2);
            $table->string('product_id');
            $table->foreign('product_id')
                ->references('_id')
                ->on('products')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->string('order_id');
            $table->foreign('order_id')
                ->references('_id')
                ->on('orders')
                ->onDelete('cascade')
                ->onUpdate('cascade');
                  //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_items');
    }
}
