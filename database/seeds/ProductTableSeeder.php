<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB ;

use App\Product;



class ProductTableSeeder extends Seeder {

	/**
	 * Run the Products table seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB :: collection ( 'products' ) -> delete ();

		DB :: collection ( 'products' ) -> insert ([ 
			'name' 			=> 'Laptop 1',
				'slug' 			=> 'Laptop-1',
				'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
				'extract' 		=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
				'price' 		=> 275.00,
				'image' 		=> 'https://media.4rgos.it/i/Argos/9164291_R_Z001A?w=750&h=440&qlt=70',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1,
			'seed' => true 
			]);
		DB :: collection ( 'products' ) -> insert ([ 
			'name' 			=> 'Laptop 2',
				'slug' 			=> 'Laptop-2',
				'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
				'extract' 		=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
				'price' 		=> 255.00,
				'image' 		=> 'https://pisces.bbystatic.com/image2/BestBuy_MX/images/products/1000/1000213127_ra.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1,
			'seed' => true 
			]);

		// $data = array(
		// 	[
		// 		'name' 			=> 'Laptop 1',
		// 		'slug' 			=> 'Laptop-1',
		// 		'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
		// 		'extract' 		=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
		// 		'price' 		=> 275.00,
		// 		'image' 		=> 'https://media.4rgos.it/i/Argos/9164291_R_Z001A?w=750&h=440&qlt=70',
		// 		'visible' 		=> 1,
		// 		'created_at' 	=> new DateTime,
		// 		'updated_at' 	=> new DateTime,
		// 		'category_id' 	=> 1
		// 	],
		// 	[
		// 		'name' 			=> 'Laptop 2',
		// 		'slug' 			=> 'Laptop-2',
		// 		'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
		// 		'extract' 		=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
		// 		'price' 		=> 255.00,
		// 		'image' 		=> 'https://pisces.bbystatic.com/image2/BestBuy_MX/images/products/1000/1000213127_ra.jpg',
		// 		'visible' 		=> 1,
		// 		'created_at' 	=> new DateTime,
		// 		'updated_at' 	=> new DateTime,
		// 		'category_id' 	=> 1
		// 	],
		// 	[
		// 		'name' 			=> 'Laptop 3',
		// 		'slug' 			=> 'Laptop-3',
		// 		'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
		// 		'extract' 		=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
		// 		'price' 		=> 300.00,
		// 		'image' 		=> 'https://pisces.bbystatic.com/image2/BestBuy_MX/images/products/1000/1000221450_sa.jpg',
		// 		'visible' 		=> 1,
		// 		'created_at' 	=> new DateTime,
		// 		'updated_at' 	=> new DateTime,
		// 		'category_id' 	=> 1
		// 	],
		// 	[
		// 		'name' 			=> 'Laptop 4',
		// 		'slug' 			=> 'Laptop-4',
		// 		'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
		// 		'extract' 		=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
		// 		'price' 		=> 475.00,
		// 		'image' 		=> 'https://pisces.bbystatic.com/image2/BestBuy_MX/images/products/1000/1000223604_sa.jpg',
		// 		'visible' 		=> 1,
		// 		'created_at' 	=> new DateTime,
		// 		'updated_at' 	=> new DateTime,
		// 		'category_id' 	=> 2
		// 	],
		// 	[
		// 		'name' 			=> 'Laptop 5',
		// 		'slug' 			=> 'Laptop-5',
		// 		'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
		// 		'extract' 		=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
		// 		'price' 		=> 280.00,
		// 		'image' 		=> 'https://pisces.bbystatic.com/image2/BestBuy_MX/images/products/1000/1000220592cv11a.jpg',
		// 		'visible' 		=> 1,
		// 		'created_at' 	=> new DateTime,
		// 		'updated_at' 	=> new DateTime,
		// 		'category_id' 	=> 2
		// 	],
		// 	[
		// 		'name' 			=> 'Laptop 6',
		// 		'slug' 			=> 'Laptop-6',
		// 		'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
		// 		'extract' 		=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
		// 		'price' 		=> 275.00,
		// 		'image' 		=> 'https://pisces.bbystatic.com/image2/BestBuy_MX/images/products/1000/1000221445_sa.jpg',
		// 		'visible' 		=> 1,
		// 		'created_at' 	=> new DateTime,
		// 		'updated_at' 	=> new DateTime,
		// 		'category_id' 	=> 2
		// 	],
		// 	[
		// 		'name' 			=> 'Laptop 7',
		// 		'slug' 			=> 'playera-7',
		// 		'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
		// 		'extract' 		=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
		// 		'price' 		=> 275.00,
		// 		'image' 		=> 'https://pisces.bbystatic.com/image2/BestBuy_MX/images/products/1000/1000223887_sa.jpg',
		// 		'visible' 		=> 1,
		// 		'created_at' 	=> new DateTime,
		// 		'updated_at' 	=> new DateTime,
		// 		'category_id' 	=> 1
		// 	],
		// 	[
		// 		'name' 			=> 'Laptop 8',
		// 		'slug' 			=> 'Laptop-8',
		// 		'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
		// 		'extract' 		=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
		// 		'price' 		=> 275.00,
		// 		'image' 		=> 'https://pisces.bbystatic.com/image2/BestBuy_MX/images/products/1000/1000213007_sa.jpg',
		// 		'visible' 		=> 1,
		// 		'created_at' 	=> new DateTime,
		// 		'updated_at' 	=> new DateTime,
		// 		'category_id' 	=> 2
		// 	],
			
		// );

		// Product::insert($data);

	}

}