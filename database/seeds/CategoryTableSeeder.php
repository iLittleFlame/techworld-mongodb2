<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\ DB ;

 
use App\Category;
 
class CategoryTableSeeder extends Seeder {
 
	/**
	 * Run the Categories table seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB :: collection ( 'categories' ) -> delete ();

		DB :: collection ( 'categories' ) -> insert ([ 
			'name' => 'Homen' ,
			'slug' => 'Laptop-Omen',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore, perferendis!', 
			'color' => '#440022', 
			'seed' => true 
			]);
		DB :: collection ( 'categories' ) -> insert ([ 
				'name' => 'Geek', 
				'slug' => 'geek', 
				'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore, perferendis!', 
				'color' => '#445500',
				'seed' => true 
			]);
		// $data = array(
		// 	[
		// 		'name' => 'Homen', 
		// 		'slug' => 'Laptop-Omen', 
		// 		'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore, perferendis!', 
		// 		'color' => '#440022'
		// 	],
		// 	[
		// 		'name' => 'Geek', 
		// 		'slug' => 'geek', 
		// 		'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore, perferendis!', 
		// 		'color' => '#445500'
		// 	]
		// );
 
		// Category::insert($data);
 
	}
}