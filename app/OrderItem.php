<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class OrderItem extends Eloquent 
{
    protected $primarykey='_id';

    protected $fillable = [
        'quantity',
        'price',
        'product_id',
        'order_id',
    ];

    public $timestamps = false;

	public function order()
	{
	    return $this->belongsTo('App\Order');
	}

	public function product()
    {
        return $this->belongsTo('App\Product');
    }
}