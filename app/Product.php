<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Product extends Eloquent 
{
    protected $primarykey='_id';

    protected $fillable = [
        'name',
        'slug',
        'description',
        'extract',
	    'price',
		'image',
        'visible',
        'category_id',
    ];

    // Relation with Category
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    // Relation with OrderItem
    public function order_item()
    {
        return $this->hasOne('App\OrderItem');
    }
}