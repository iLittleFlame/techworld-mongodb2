<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Category extends Eloquent 
{
    protected $primarykey='_id';

    protected $fillable = [
        'name',
		'slug',
		'description',
		'color',
    ];

    public $timestamps = false;
    
    public function products()
    {
        return $this->hasMany('App\Product');
    }
}