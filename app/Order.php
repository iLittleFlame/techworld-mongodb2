<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class OrderItem extends Eloquent 
{
    protected $primarykey='_id';

    protected $fillable = [
        'subtotal',
        'shipping',
        'user_id',
    ];

    // Relation with User
	public function user()
	{
	    return $this->belongsTo('App\User');
	}

	public function order_items()
	{
	    return $this->hasMany('App\OrderItem');
	}
}