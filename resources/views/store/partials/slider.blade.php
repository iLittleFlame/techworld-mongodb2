<div id="slider" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#slider" data-slide-to="0" class="active"></li>
    <li data-target="#slider" data-slide-to="1"></li>
    <li data-target="#slider" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="https://jamesaj.com/wp-content/uploads/2019/01/4.png" alt="slide1">
        <div class="carousel-caption">
          Slide 1
        </div>
      </div>
      <div class="item">
        <img src="https://xpc.com.ec/clientes/wp-content/uploads/Banner-Web-XPC-01-002.jpg" alt="slide2">
        <div class="carousel-caption">
          Slide 2
        </div>
      </div>
      <div class="item">
        <img src="https://www.infotec.com.pe/modules/ps_imageslider/images/a3a5d08408c88a07bd702406a1aea92382e6968e_Banner-Web-Infotec.jpg" alt="slide3">
        <div class="carousel-caption">
          Slide 3
        </div>
      </div>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#slider" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#slider" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div><hr>