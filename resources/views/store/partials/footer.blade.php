<footer class="container-fluid">
    <div class="row">
        <div class="col-md-4">
            <h3>TechWold</h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt, ducimus. 
                Cum quis fugiat praesentium nisi corporis, at debitis id quas voluptate? Quaerat tenetur,
                 cumque sit sint natus facere quod at!</p>
        </div>
        <div class="col-md-4">
            <h3>Desarrollado por:</h3>
            <img src="http://www.mundociencia.com/wp-content/uploads/2017/08/Dise%C3%B1o-sin-t%C3%ADtulo-53.jpg" 
            alt="" class="avatar">
            <p><a href="#"></a> Lorem, ipsum dolor sit amet consectetur adipisicing elit. Asperiores, labore accusantium blanditiis 
                quae praesentium adipisci accusamus dolores natus tempore nulla eaque dolore ab
                 vitae totam, sunt fugiat veritatis nesciunt illum quia at et ea, delectus quas inventore! 
                 Culpa, perferendis asperiores!</p>
        </div>
        <div class="col-md-4">
            <h3>Siguenos</h3>
            <ul class="redes">
                <li>
                    <a href="#"><i class="fa fa-facebook-square fa-2x"></i></a>
                </li>
            </ul>
            <h3>Escribenos:</h3>
            <i class="fa fa-at"><a href="#">pedr0234@hotmail.com</a></i>
        </div>
    </div>
</footer>